#include <stdio.h>
#include <stdlib.h>
#include<string.h>

using namespace std;

int readMatrix(int **matrix,int rows,int cols);
void printMatrix(int **matrix,int rows,int cols);
void freeMatrix(int **matrix,int rows);
int countX(int x,int **matrix, int rows, int cols);
int listX(int x,int **matrix, int rows, int cols);
/*----------------------------------------------------*/
int readMatrix(int **matrix,int rows,int cols)
{
    int i,j,x;
    printf("Cenova mapa:\n");
    for(i=0;i<rows;i++)
        for(j=0;j<cols;j++)
        {
            if(scanf("%d", &x)!=1)
                return 0;
            matrix[i][j]=x;
        }
    return 1;
}
/*----------------------------------------------------*/
void printMatrix(int **matrix,int rows,int cols)
{
int i,j;
for(i=0;i<rows;i++)
 {
  for (j=0;j<cols;j++)
  {
   printf("%d ", matrix[i][j]);
  }
 printf("\n");
 }
}
/*----------------------------------------------------*/
void freeMatrix(int **matrix,int rows)
{
int i;
for(i=0;i<rows;i++)
 {
   free(matrix[i]);
  }
  free(matrix);
 }
/*----------------------------------------------------*/
int listX(int x,int **matrix, int rows, int cols)
{
    int i,j,k,l,m,soucet=0, res, xcount=0;
for (m=0;m<cols;m++)
{
    for (i=0;i<rows;i++)
    {
        soucet=0;
    for (j=m;j<cols;j++)
    {
        //printf("%d", matrix[i][j]);
        soucet+=matrix[i][j];
        res=soucet;
        if (res==x)
            {
                printf("%d @ (%d,%d) - (%d,%d)\n", x,m, i, j, i);
                xcount++;
            }
        //printf("-(%d) ", res);
        for (k=i+1;k<rows;k++)
        {
            //res+=matrix[k][j];
           //printf("%d-(%d) \n", matrix[k][j], res);
            for (l=m;l<=j;l++)
            {
                res+=matrix[k][l];
            }
            if (res==x)
            {
                printf("%d @ (%d,%d) - (%d,%d)\n", x,m, i, j, k);
                xcount++;
            }
        }
    }
    }
}
    return xcount;
}
/*----------------------------------------------------*/
int countX(int x,int **matrix, int rows, int cols)
{
    int i,j,k,l,m,soucet=0, res, xcount=0;
for (m=0;m<cols;m++)
{
    for (i=0;i<rows;i++)
    {
        soucet=0;
    for (j=m;j<cols;j++)
    {
        soucet+=matrix[i][j];
        res=soucet;
        if (res==x)
            {
                xcount++;
            }
        for (k=i+1;k<rows;k++)
        {
            for (l=m;l<=j;l++)
            {
                res+=matrix[k][l];
            }
            if (res==x)
            {
                xcount++;
            }
        }
    }
    }
}
    return xcount;
}
/*----------------------------------------------------*/
int main()
{
    int cols,rows,number;
    int **matrix;
    int i=0, xcount=0;
    char text[100];
    char num[100];
    //nacteni rozmeru mapy
    printf("Velikost mapy:\n");
    if(scanf("%d %d", &cols, &rows)!=2 || cols<1 ||rows<1 || cols>2000 || rows>2000)
    {
        printf("Nespravny vstup.\n");
        return 1;
    }
    matrix=(int**)malloc(rows*sizeof(*matrix));
    if (!matrix) return 0;

    for(i=0;i<rows;i++)
        {
            matrix[i]=(int*)malloc(cols* sizeof(*matrix[i]));
            if (!matrix[i])
                {
                    freeMatrix ( matrix, i );
                    return 0;
                }
        }
    if(!readMatrix(matrix,rows,cols))
    {
        printf("Nespravny vstup.\n");
        freeMatrix(matrix,i);
        return 0;
    }
    printf("Dotazy:\n");

    while(scanf("%99s %99s", text, num)!=-1)
    //scanf("%99s %99s", text, num);
    {
    number=atoi(num);
    if (number || *num=='0')
    {
        if(!strcmp("list", text))
            {xcount=listX(number,matrix,rows,cols);
            printf("Celkem: %d\n", xcount);}
        else if(!strcmp("count", text))
            {xcount=countX(number,matrix,rows,cols);
            printf("Celkem: %d\n", xcount);}
        else
            {
                printf("Nespravny vstup.\n");
                return 0;}
    }
    else
        {printf("Nespravny vstup.\n");
        return 0;}
    }



    freeMatrix(matrix,rows);
    return 0;
}
